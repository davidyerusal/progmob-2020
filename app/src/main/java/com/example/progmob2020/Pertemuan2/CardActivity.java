package com.example.progmob2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class CardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        //CardView cv = (CardView)findViewById(R.id.cvMahasiswa);

        //RecyclerView rv = (RecyclerView)findViewById(R.id.rvGetDsnAll);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvGetMhsAll);

        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;

        //Data
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate
        Mahasiswa m1 = new Mahasiswa("David", "72180267", "0778");
        Mahasiswa m2 = new Mahasiswa("Jojo", "72180123", "0777");
        Mahasiswa m3 = new Mahasiswa("kaka", "72180456", "0776");
        Mahasiswa m4 = new Mahasiswa("Keke", "72180789", "0775");
        Mahasiswa m5 = new Mahasiswa("banu", "72180098", "0774");


        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(CardActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        //cv.setLayoutManager

        rv.setLayoutManager(new LinearLayoutManager(CardActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}