package com.example.progmob2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);


        EditText edKodelama = (EditText)findViewById(R.id.editTextKodeCari);
        EditText edNamamt = (EditText)findViewById(R.id.editTextNamaMatkulBaru);
        EditText edKode = (EditText)findViewById(R.id.editTextKodeBaru);
        EditText edhari = (EditText)findViewById(R.id.editTextHariBaru);
        EditText edsesi = (EditText)findViewById(R.id.editTextSesiBaru);
        EditText edsks = (EditText)findViewById(R.id.editTextSksBaru);
        Button btnupdate = (Button)findViewById(R.id.buttonUbahMatkul);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        Intent data = getIntent();
        if(data.getExtras() != null){
            edNamamt.setText(data.getStringExtra("nama"));
            edKode.setText(data.getStringExtra("kode"));
            edhari.setText(data.getStringExtra("hari"));
            edsesi.setText(data.getStringExtra("sesi"));
            edsks.setText(data.getStringExtra("sks"));
            edKodelama.setText(data.getStringExtra("kode"));
        }

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.update_matkul(
                        edKodelama.getText().toString(),
                        edNamamt.getText().toString(),
                        edKode.getText().toString(),
                        edhari.getText().toString(),
                        edsesi.getText().toString(),
                        edsks.getText().toString(),

                        "72180267"
                        //ednidnlama.getText().toString()
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MatkulUpdateActivity.this, MainMatkulActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this, "Data Tidak Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}