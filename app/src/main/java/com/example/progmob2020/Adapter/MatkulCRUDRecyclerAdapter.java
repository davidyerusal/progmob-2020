package com.example.progmob2020.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Crud.MatkulUpdateActivity;
import com.example.progmob2020.Model.Matkul;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> matkulList;

    public MatkulCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMataKuliahList() {
        return matkulList;
    }

    public void setMataKuliahList(List<Matkul> matakuliahList) {
        this.matkulList = matakuliahList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Matkul mk = matkulList.get(position);

        holder.tvNama.setText(mk.getNama());
        holder.tvKode.setText(mk.getKode());
        holder.tvHari.setText(mk.getHari());
        holder.tvSesi.setText(mk.getSesi());
        holder.tvSks.setText((mk.getSks()));
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvKode, tvNama, tvHari, tvSesi, tvSks;
        private RecyclerView rvMatkulGetAll;
        Matkul mt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNamaMatkul);
            tvKode = itemView.findViewById(R.id.tvKode);
            tvHari = itemView.findViewById(R.id.tvHari);
            tvSesi = itemView.findViewById(R.id.tvSesiMatkul);
            tvSks = itemView.findViewById(R.id.tvSksMatkul);
            rvMatkulGetAll = itemView.findViewById(R.id.rvGetMatkulAll);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(itemView.getContext(), MatkulUpdateActivity.class);
                    intent.putExtra("kode",mt.getKode());
                    intent.putExtra("nama",mt.getNama());
                    intent.putExtra("hari",mt.getHari());
                    intent.putExtra("sesi",mt.getSesi());
                    intent.putExtra("sks",mt.getSks());

                    //intent.putExtra("alamat",m.getAlamat());
                    //intent.putExtra("email",m.getEmail());

                    itemView.getContext().startActivity(intent);
                }
            });

        }
    }
}